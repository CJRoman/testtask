requirejs.config({
    baseUrl: 'js',
    paths: {
      jquery: 'https://code.jquery.com/jquery-2.1.4.min',
    }
});

requirejs(['jquery', 'app/main']);