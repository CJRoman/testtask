(function($) {

  var defaultLocation = '#persons',
      currentLocation = window.location.hash || defaultLocation,
      pageTitle;

  /**
   * showMainContainer shows all page content when all is loaded
   */
  function showMainContainer() {
    $("#mainContainer").removeClass("hide");
    renderPageTitle();
  }

  /**
   * hashChange sets current page and fires entity reload
   */
  function hashChange() {
    currentLocation = window.location.hash || defaultLocation;
    renderPageTitle();
  };

  /**
   * renderPageTitle sets h1#pageTitle tag on page same as hash 
   */
  function renderPageTitle () {
    pageTitle = currentLocation.slice(1).charAt(0).toUpperCase() + currentLocation.slice(2);
    $("#pageTitle").text(pageTitle);
  }
  
  $(document).ready(showMainContainer);
  $(window).on('hashchange', hashChange);
})($);